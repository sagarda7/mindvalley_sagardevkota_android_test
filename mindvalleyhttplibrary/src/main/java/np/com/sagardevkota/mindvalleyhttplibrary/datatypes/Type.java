package np.com.sagardevkota.mindvalleyhttplibrary.datatypes;

import np.com.sagardevkota.mindvalleyhttplibrary.listeners.HttpListener;
import np.com.sagardevkota.mindvalleyhttplibrary.utils.CacheManager;
import np.com.sagardevkota.mindvalleyhttplibrary.utils.CacheManagerInterface;

/**
 * Created by HP on 7/12/2016.
 */
public abstract class Type<T> {
    public abstract Type setCacheManager(CacheManagerInterface<T> cacheManager);
    public abstract Type setCallback(HttpListener<T> callback);
    public abstract boolean cancel();
}
